import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.UUID;

/**
 * @author hy
 * @since 2021/11/27
 */
public class JedisPoolTests {
    @Test
    public void testJedisPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(1000);
        config.setMaxIdle(60);

        JedisPool jedisPool = new JedisPool("176.8.2.6", 6379);
        Jedis resource = jedisPool.getResource();
        resource.set("Authorization", UUID.randomUUID().toString());
        resource.expire("Authorization", 60);
        resource.close();
        jedisPool.close();



    }
}
