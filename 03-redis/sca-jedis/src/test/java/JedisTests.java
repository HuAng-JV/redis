import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author hy
 * @since 2021/11/27
 */
public class JedisTests {
    @Test
    public void testConnection() {
        Jedis jedis = new Jedis("176.8.2.6", 6379);
        String ping = jedis.ping();
        System.out.println(ping);
    }

    @Test
    public void testInsertToken() {
        Jedis jedis = new Jedis("176.8.2.6", 6379);
        jedis.set("Authorization", UUID.randomUUID().toString());
        jedis.expire("Authorization", 60);
        Map<String, String> map = new HashMap<>();
        map.put("Authorization", UUID.randomUUID().toString());
        String json = new Gson().toJson(map);
        jedis.set("token", json);
        jedis.expire("token", 60);
        jedis.close();
    }
}
