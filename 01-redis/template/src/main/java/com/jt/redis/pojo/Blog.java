package com.jt.redis.pojo;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author hy
 * @since 2021/11/11
 */

public class Blog implements Serializable{

    private Integer id;

    private String title;

    public Blog(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    public Blog() {
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Blog blog = (Blog) object;
        return Objects.equals(id, blog.id) &&
                Objects.equals(title, blog.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
