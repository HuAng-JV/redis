package com.jt.redis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author hy
 * @since 2021/11/12
 */
@Configuration
public class CachingConfig extends CachingConfigurerSupport {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 重构CacheManager对象
     */
    @Bean
    @Primary
    @Override
    public CacheManager cacheManager() {
        //定义RedisCache配置
        RedisCacheConfiguration cacheConfig = RedisCacheConfiguration.defaultCacheConfig();
        //定义key的序列化方式
        cacheConfig.serializeKeysWith(
                RedisSerializationContext.SerializationPair
                        .fromSerializer(RedisSerializer.string()))
        //定义value的序列化方式
                   .serializeValuesWith(
                RedisSerializationContext.SerializationPair
                        .fromSerializer(RedisSerializer.json()));

        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults(cacheConfig)
                .build(); //建造者模式(复杂对象的创建，简易使用这种方式，封装了对象的创建细节)
    }
}
