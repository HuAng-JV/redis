package com.jt.redis.service.impl;

import com.jt.redis.mapper.MenuMapper;
import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.Duration;

/**
 * 实现类
 * @author hy
 * @since 2021/11/11
 */
@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

//    @Autowired
//    private RedisTemplate redisTemplate;

    @Resource(name = "redisTemplate")
    private ValueOperations vo;


    /**
     * 基于id查询菜单信息,要求:
     * 1)先查redis,redis没有去查mysql
     * 2)将从mysql查询到的数据存储到redis
     * @param id
     * @return
     */
    @Override
    public Menu selectById(Long id) {
        //ValueOperations vo = redisTemplate.opsForValue();
        Object obj = vo.get(id);
        if (obj != null) {
            System.out.println("get data from redis");
            return (Menu)obj;
        }
        Menu menu = menuMapper.selectById(id);
        vo.set(id,menu, Duration.ofSeconds(60));
        System.out.println("get data from mysql");
        return menu;
    }

    @Override
    public Menu insertMenu(Menu menu) {
        menuMapper.insert(menu);
        //ValueOperations vo = redisTemplate.opsForValue();
        vo.set(String.valueOf(menu.getId()), menu, Duration.ofSeconds(120));
        return menu;
    }
    @Override
    public Menu updateMenu(Menu menu) {
        menuMapper.updateById(menu);
        //ValueOperations vo = redisTemplate.opsForValue();
        vo.set(String.valueOf(menu.getId()), menu, Duration.ofSeconds(120));
        return menu;
    }
}
