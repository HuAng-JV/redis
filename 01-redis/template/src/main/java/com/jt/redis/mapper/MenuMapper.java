package com.jt.redis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.redis.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hy
 * @since 2021/11/11
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

}
