package com.jt.redis.service.impl;

import com.jt.redis.mapper.MenuMapper;
import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author hy
 * @since 2021/11/11
 */
@Service
public class DefaultMenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Cacheable(value = "menuCache", key = "#id")
    @Override
    public Menu selectById(Long id) {
        return menuMapper.selectById(id);
    }

//    @CachePut(value = "menuCache", key = "#menu.id")
    @Override
    public String insertMenu(Menu menu) {
        menuMapper.insert(menu);
        return "insert ok";
    }

//    @CachePut(value = "menuCache", key = "#menu.id")
    @Override
    public String updateMenu(Menu menu) {
        menuMapper.updateById(menu);
        return "update ok";
    }
}
