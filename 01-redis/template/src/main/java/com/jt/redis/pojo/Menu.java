package com.jt.redis.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author hy
 * @since 2021/11/11
 */
@Data
@Accessors(chain = true)
@TableName(value = "tb_menus")
public class Menu implements Serializable {
    private static final long serialVersionUID = -4957781116270624916L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String permission;

}
