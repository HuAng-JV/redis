package com.jt.redis;

import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author hy
 * @since 2021/11/12
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Resource(name = "defaultMenuServiceImpl")
    private MenuService menuService;

    @GetMapping("/{id}")
    public Object selectId(@PathVariable("id") Long id) {
        return menuService.selectById(id);
    }

    @PostMapping("/saveMenu")
    public Object saveMenu(@RequestBody Menu menu) {
        return menuService.insertMenu(menu);
    }

    @PutMapping("/updateMenu")
    public Object updateMenu(@RequestBody Menu menu) {
        return menuService.updateMenu(menu);
    }
}
