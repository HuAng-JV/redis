package com.jt.redis;

import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


/**
 * @author hy
 * @since 2021/11/11
 */
@SpringBootTest
public class MenuQueryTests {
//    @Autowired
//    private MenuService menuService;

    @Resource(name = "defaultMenuServiceImpl")
    private MenuService menuService;


    @Test
    void testFindMenu() {
        Menu menu = menuService.selectById(1L);
        System.out.println(menu);
    }

    @Test
    void testUpdateMenu(){
        Menu menu = menuService.selectById(1L);
        menu.setName("select res");
        menuService.updateMenu(menu);
    }
    @Test
    void testInertMenu(){
        Menu menu = new Menu();
        menu.setName("insert res");
        menu.setPermission("sys:res:insert");
        menuService.insertMenu(menu);
    }
}
