package com.jt.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * @author hy
 * @since 2021/11/14
 */
@SpringBootTest
public class MasterSlaveTests {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testMasterReadWrite() {//配置文件端口为6379
        ValueOperations vo = redisTemplate.opsForValue();
        vo.set("role", "master6379");
        Object role = vo.get("role");
        System.out.println(role);
    }

    @Test
    void testSlaveRead() { //配置端口为6380/6381
        ValueOperations vo = redisTemplate.opsForValue();
        Object role = vo.get("role");
        System.out.println(role);

    }

}
