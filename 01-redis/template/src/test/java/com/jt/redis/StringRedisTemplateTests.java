package com.jt.redis;

import com.jt.redis.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;


/**
 * @author hy
 * @since 2021/11/10
 */
@SpringBootTest
public class StringRedisTemplateTests {
    /**
     * 可以基于此对象实现与redis数据库的交互，它
     * 会以字符串序列化的方式存储key/value
     * 序列化/反序列化
     * 狭义：序列化->将对象转换为字节
     *      反序列化->将字节转换为对象
     * 广义：序列化->将对象转换为json格式字符串或字节
     *      反序列化->将字节或json串转换为对象
     */
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testGetConnection() {
//        RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
//        String ping = connection.ping();
//        System.out.println(ping);

//        ValueOperations<String, String> vo = redisTemplate.opsForValue();
//        vo.set("name", "redis");
//        vo.set("author", "tony", 10); //10表示有效时长
//        String name = vo.get("name");
//        System.out.println(name);

        ValueOperations vo = redisTemplate.opsForValue();
        Blog blog = new Blog();
        blog.setId(100);
        blog.setTitle("redis in java");
        vo.set("blog", blog);
        blog = (Blog) vo.get("blog");
        System.out.println(blog);
    }
}
