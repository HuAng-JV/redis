import com.jt.redis.JedisDataSource;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Jedis连接池的应用测试
 * 享元模式：它设计思想，通过池减少对象的创建次数实现对象的可重用性，所有的池的设计
 * 都有这个设计模式的应用(例如整数池，字符串池，线程池，连接池)
 * Jedis连接池(与Redis数据库的连接池)的应用测试
 * @author hy
 * @since 2021/11/10
 */

public class JedisPoolTests {
    @Test
    public void testIntegerCache() {
        //1、构建池对象
        //1.1、构建连接池配置(可选，不写有默认的)
        //2、从池中获取连接
        Jedis resource = JedisDataSource.getConnection();
        //3、基于连接实现数据读写
        resource.hset("blog", "id", "100");
        String value = resource.hget("blog", "id");
        System.out.println(value);
        //4、释放资源
        resource.close(); //将资源还回池中
    }
}
