import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author hy
 * @since 2021/11/9
 */
public class JedisTests {
    private static final String IP = "192.168.126.128";
    private static final int PORT = 6379;

    @Test
    public void testString01() {
        Jedis jedis = new Jedis(IP,PORT);
        jedis.set("id", "100");
        jedis.set("token", UUID.randomUUID().toString());
        jedis.expire("token", 2);
        jedis.incr("id");
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", "201");
        map.put("name", "redis");
        Gson gson = new Gson();
        String jsonStr = gson.toJson(map); //将map对象转换为JSON字符串
        jedis.set("lession", jsonStr);
        //释放资源
        jedis.close();
    }



    @Test
    public void testGetConnection() {
        Jedis jedis = new Jedis(IP, PORT);
        String ping = jedis.ping();
        System.out.println(ping);
    }
}
