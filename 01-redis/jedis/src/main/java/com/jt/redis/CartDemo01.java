package com.jt.redis;

import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * @author hy
 * @since 2021/11/11
 */
public class CartDemo01 {
    private static final String ip = "176.8.2.6";
    private static final int port = 6379;
    private static final String prefix = "cart:";

    static void addCart(String userId, Long productId,int num) {
        //建立连接
        Jedis jedis = new Jedis(ip, port);
        //添加商品
        jedis.hincrBy(prefix + userId,
                String.valueOf(productId),
                num);
        //释放资源
        jedis.close();
    }

    static Map<String, String> listCart(String userId) {
        //建立连接
        Jedis jedis = new Jedis(ip, port);
        //获取购物车信息
        Map<String, String> map = jedis.hgetAll(prefix + userId);
        //释放资源
        jedis.close();
        return map;
    }

    public static void main(String[] args) {
        //向购物车添加商品
        addCart("101", 2001L, 1);
        addCart("102", 2002L, 1);
        //查看购物车商品
        Map<String, String> map = listCart("101");
        System.out.println(map);
        //修改购物车商品数量
    }
}
