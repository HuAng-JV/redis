package com.jt.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author hy
 * @since 2021/11/10
 */
public class JedisDataSource {

    private static final String IP = "192.168.126.128";
    private static final int PORT = 6379;
    /**
     * volatile 关键通常用于修饰属性：
     * 1、保证线程其可见性（一个线程改了，其他CPU线程立刻可见）
     * 2、不能保证其原子性（不保证线程安全）
     * 3、禁止指令重排序(count++，int temp =count)
     */
    private static volatile JedisPool jedisPool;
    //方案1、饿汉式池对象的创建
    static {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(16);
        config.setMaxIdle(8);
        jedisPool = new JedisPool(config,IP,PORT);
    }

    public static Jedis getConnection() {
        return jedisPool.getResource();
    }

    //方案2、懒汉式池对象的创建
    public static Jedis getConnection1() {
        if (jedisPool == null) {
            synchronized(JedisDataSource.class) {
                if (jedisPool == null) {
                    JedisPoolConfig config = new JedisPoolConfig();
                    config.setMaxTotal(16);
                    config.setMaxIdle(8);
                    jedisPool = new JedisPool(config, IP, PORT);
                    //创建对象分析
                    //1、开辟内存空间
                    //2、执行属性的默认初始化
                    //3、执行构造方法
                    //4、将创建的对象的内存地址赋值给jedisPool变量
                    //假如使用了volatile修饰jedisPool变量，可以保证如上几个步骤是顺序
                }
            }
        }
        return jedisPool.getResource();
    }

}
