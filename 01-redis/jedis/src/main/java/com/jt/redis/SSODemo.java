package com.jt.redis;

import redis.clients.jedis.Jedis;
import java.util.UUID;

/**
 * 基于Redis的单点登录设计实现
 * 1、用户登录成功以后将登录状态等信息存储到Redis
 * 2、用户携带token去访问资源，资源服务 器要基于token从redis查询用户信息
 * @author hy
 * @since 2021/11/10
 */
public class SSODemo {
    /**
     * 执行登录认证，将来这样的业务要写到认证服务器
     * @param username
     * @param password
     */
    static String login(String username, String password) {
        //1、校验数据的合法性(判定用户名密码是否为空，密码的长度，是否有数字字母等特殊符号)
        if (username == null || "".equals(username)) {
            throw new IllegalArgumentException("用户名不能为空");
        }
        //2、基于用户名查询用户信息，并判断密码是否正确
        if (!"jack".equals(username)) {
            throw new RuntimeException("用户不存在");
        }
        if (!"123456".equals(password)) {
            throw new RuntimeException("密码错误");
        }
        //3、用户名存在且密码正确，将用户信息写入到redis
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        String token = UUID.randomUUID().toString();
        jedis.hset(token,"username", username);
        jedis.expire(token, 10);
        jedis.hset(token,"permission", "1");
        //4、将token返回给客户端(将来使用response对象响应到客户端)
        return token;


    }
    static String token;

    /**
     * 演示资源访问过程
     * 1、允许匿名访问(无需登录)
     * 2、登录后访问(认证通过了)
     * 3、登录后必须有权限才能访问
     * @param
     */
    static String getResource(String token) {
        //1、校验token有效性
        if (token == null || "".equals(token)) {
            throw  new IllegalArgumentException("请先登录");
        }
        //2、基于token查询redis数据，假如有对应数据说明用户登录了
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        String username = jedis.hget(token, "username");
        if (username == null) {
            throw new RuntimeException("登录超时，请重新登录");
        }
        //3、检查用户是否有访问资源的权限，假如有则允许访问
        String permission = jedis.hget(token, "permission");
        if (!"1".equals(permission)) {
            new RuntimeException("没有权限访问资源");
        }
        //4、返回要访问的资源
        return "your resource";
    }

    public static void main(String[] args) {
        //1、登录操作(用户身份认证)
        token = login("jack", "123456");
        System.out.println(token);
        //2、携带token访问资源服务器
        System.out.println(getResource(token));
    }
}
