package com.jt.redis;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 基于某个活动的简易投票系统设计
 * 1、投票数据存储到redis（key为活动id，值为多个用户id集合）
 * 2、同一个用户不能执行多次投票
 * 3、具体业务操作（投票，获取总票数，检查是否投过票，取消投票，获取那些人参与了投票）
 * @author hy
 * @since 2021/11/10
 */
public class VoteDemo {
    /**
     * 执行投票操作
     * @param activityId
     * @param userId
     */
    static void doVote(String activityId, String userId) {
        //1、建立连接
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        //执行投票
        //检查是否投过票
        Boolean flag = jedis.sismember(activityId, userId);
        if (flag) {
            //假如已经投票，再投票就取消
            jedis.srem(activityId, userId);
        } else {
            //没有投票则执行投票
            jedis.sadd(activityId, userId);
        }
    }

    public static void main(String[] args) {
        String activityId = "ww";
        String userId = "11";
        String userId1 = "30";
        String userId2 = "23";
        //执行投票
        doVote(activityId, userId);
        doVote(activityId, userId1);
        doVote(activityId, userId2);
        //获取总票数
        Long aLong = doCount(activityId);
        System.out.println("总票数:" + aLong);
        //获取参与投票的成员
        Set<String> members = doGetMembers(activityId);
        System.out.println("投票成员:" + members);

    }


    static Long doCount(String activityId) {
        //1、建立连接
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        //2、获取当前活动的总数
        Long count = jedis.scard(activityId);
        //3、释放资源
        jedis.close();
        return count;
    }

    /**
     * 哪些人执行了这个活动的投票
     * @param activityId
     * @return
     */
    static Set<String> doGetMembers(String activityId) {
        //1、建立连接
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        //2、获取参与投票的成员集合
        Set<String> members = jedis.smembers(activityId);
        //3、释放资源
        jedis.close();
        return members;
    }
}
